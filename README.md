# DFL GammaEffects

GammaEffects is a class to handle various display effects that can be performed using gamma control.
Currently only color temperature effects, and brightness can be set using this library.


### Dependencies:
* <tt>Qt5 Core and Gui (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>
* <tt>[WayQt](https://gitlab.com/desktop-frameworks/wayqt.git</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/gamma-effects.git dfl-gammaeffects`
- Enter the `dfl-gammaeffects` folder
  * `cd dfl-gammaeffects`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any feature that you ask for.
